package telianova02;

import java.util.Random;
public class Main {

	public static void main(String[] args) {printTable();}

    private static void printTable() {
        Random random = new Random();
        System.out.println(" Number\t\t| Pattern: 01 | Pattern: 10| ");
        System.out.println("--------------------------------------------|");
        for (int i = 0; i < 10; i++){
            int num = random.nextInt();
            int pattern01 = countBinaryCombination(num, 0, 1);
            int pattern10 = countBinaryCombination(num, 1, 0);
            System.out.printf("%s\t|%12s |%12s |\n", num, pattern01, pattern10);
        }
        System.out.println("--------------------------------------------|");
    }
    private static int countBinaryCombination(int num, int firstDigit, int secondDigit) {
        if (num < 0){
            num = -num;
        }
        int counter = 0;
        int prevDigit = num % 2;
        int currDigit;
        while (num > 0) {
            currDigit = num % 2;
            if (currDigit == firstDigit && prevDigit == secondDigit)
                counter++;
            prevDigit = currDigit;
            num /=2;
        }
        return counter;
    }

}

		
	


