package telianova06;
import java.util.Comparator;

public class LengthSort implements Comparator<String> {
    @Override
    public int compare(final String o1, final String o2) {
        return Integer.compare(o1.length(), o2.length());
    }
}
