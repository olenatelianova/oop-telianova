package telianova09;
	import java.io.Serializable;
	import telianova07.JobRequirements;
	import telianova07.WorkExperience;
public class Applicant  implements Serializable{

	    private static final long serialVersionUID = -8290634946232397672L;

	    private int registrationNum;
	    private String education;
	    private int dismissalDay;
	    private int dismissalMonth;
	    private int dismissalYear;
	    private JobRequirements jobRequirements;
	    private WorkExperience workExperience;

	    /**
	     * Конструктор
	     * @param registrationNum ID претндента
	     * @param education образование претендента
	     * @param dismissalDay день увольнения претендента
	     * @param dismissalMonth месяц увольнения претендента
	     * @param dismissalYear год увольнения претендента
	     * @param workExperience опыт работы претендента
	     * @param jobRequirement пожелания к будующей работе
	     */

	    public Applicant(int registrationNum, String education, int dismissalDay, int dismissalMonth, int dismissalYear, WorkExperience workExperience, JobRequirements jobRequirements ) {
	        this.registrationNum = registrationNum;
	        this.education = education;
	        this.dismissalDay = dismissalDay;
	        this.dismissalMonth = dismissalMonth;
	        this.dismissalYear = dismissalYear;
	        this.workExperience = workExperience;
	        this.jobRequirements = jobRequirements;
	    }
	    public Applicant()
	    {
	        super();
	    }
	    /**
	     * Геттер ID претендента
	     * @return ID претендента
	     */

	    public int getRegistrationNum() {
	        return registrationNum;
	    }
	    /**
	     * Сеттер ID претендента
	     * @param registrationNum ID претендента
	     */

	    public void setRegistrationNum(int registrationNum) {
	        this.registrationNum = registrationNum;
	    }
	    /**
	     * Геттер образования претендента
	     * @return образование претендента
	     */

	    public String getEducation() {
	        return education;
	    }
	    /**
	     * Сеттер образования претендента
	     * @param education Образование претендента
	     */

	    public void setEducation(String education) {
	        this.education = education;
	    }
	    /**
	     * Геттер дня увольнения
	     * @return день увольнения
	     */

	    public int getDismissalDay() {
	        return dismissalDay;
	    }
	    /**
	     * Сеттер дня увольнеия
	     * @param dismissalDay день увольнения
	     */

	    public void setDismissalDay(int dismissalDay) {
	        this.dismissalDay = dismissalDay;
	    }
	    /**
	     * Геттер месяца увольнения
	     * @return месяц увольнения
	     */

	    public int getDismissalMonth() {
	        return dismissalMonth;
	    }
	    /**
	     * Сеттер месяца увольнения
	     * @param dismissalMonth месяц увольнения
	     */

	    public void setDismissalMonth(int dismissalMonth) {
	        this.dismissalMonth = dismissalMonth;
	    }
	    /**
	     * Геттер года увольнения претендента
	     * @return год увольнения
	     */

	    public int getDismissalYear() {
	        return dismissalYear;
	    }
	    /**
	     * Сеттер года увольнения претендента
	     * @param dismissalYear год увольнения
	     */

	    public void setDismissalYear(int dismissalYear) {
	        this.dismissalYear = dismissalYear;
	    }
	    /**
	     * Геттер опыта работы претендента
	     * @return
	     */
	    public WorkExperience getWorkExperience() {
	        return workExperience;
	    }
	    /**
	    * Сеттер опыта работы претендента
	    * @param workExperience
	    */
	    public void setWorkExperience(WorkExperience workExperience) {
	        this.workExperience = workExperience;
	    }
	    /**
	     * Геттер требований к будующей работе
	     * @return
	     */

	    public JobRequirements getJobRequirements() {
	        return jobRequirements;
	    }
	    
	    /**
	     * Сеттер требований к будующей работе
	     * @param demandsToWork
	     */

	    public void setJobRequirements(JobRequirements jobRequirements) {
	        this.jobRequirements = jobRequirements;
	    }

	    public void print() {
	        System.out.println("ID: " + getRegistrationNum());
	        System.out.println("Education: " + getEducation());
	        System.out.println("Release date: " + getDismissalDay()+"/" + getDismissalMonth()+"/"+getDismissalYear());
	        System.out.println("Experience : ");
	        System.out.println("Place of past work: " + getWorkExperience().getSpecialization());
	        if(getWorkExperience().getExperience() <= 4)
	            System.out.println("Experience: " + getWorkExperience().getExperience() + " year");
	        else
	            System.out.println("Experience: " + getWorkExperience().getExperience() + " years");
	        System.out.println("Desire for work" );
	        if(getJobRequirements().getMinSalary() == 0 && getJobRequirements().getSpecialization() == null && getJobRequirements().getConditions() == null)
	            System.out.println("The candidate does not have any aspirations for future work");
	        else {
	            if(getJobRequirements().getMinSalary() != 0)
	                System.out.println("Desired minimum salary: " + getJobRequirements().getMinSalary());
	            else
	                System.out.println("Desired minimum salary: The candidate does not have any wishes regarding the salary " );
	            if(getJobRequirements().getSpecialization() != null)
	                System.out.println("Desired future work: " + getJobRequirements().getSpecialization());
	            else
	                System.out.println("Desired future work: The applicant does not have any wishes for the job");
	            if(getJobRequirements().getConditions() != null)
	                System.out.println("Desired conditions of future work: " + getJobRequirements().getConditions());
	            else
	                System.out.println("Desired conditions of future work: The applicant does not have any wishes for the job");
	        }
	        System.out.println("------------------------------------------");
	    }
	}


