package telianova05;
import java.util.Comparator;

public class LengthSort implements Comparator<String>{
    @Override
    public int compare(final String o1, final String o2) {
        if (o1.length() > o2.length()) {
            return 1;
        } else {
            if (o1.length() < o2.length()) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}
