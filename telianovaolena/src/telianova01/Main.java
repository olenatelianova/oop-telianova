package telianova01;

public class Main {

	public static void main(String[] args) {
		final int NUMBER_OF_THE_SCOREBOOK = 0x14_E5;
		final long PHONE_NUMBER = 380500822940L;
		final int LAST_2_DIGITS_OF_NUMBER = 0b1011110;
		final int LAST_4_DIGITS_OF_NUMBER = 4366;
		final int STUDENT_NUMBER = (18 - 1) % 26 + 1;
		final char ALPHABET_SYMBOL ='R';
		long[] arr = new long[] {
				(long) NUMBER_OF_THE_SCOREBOOK, PHONE_NUMBER,
				(long) LAST_2_DIGITS_OF_NUMBER, (long) LAST_4_DIGITS_OF_NUMBER,
				(long) STUDENT_NUMBER, (long) ALPHABET_SYMBOL};
		

		int odd =0;
		int ones = odd;
		int even = ones;
	
		
		for(long el: arr) {
			
			ones+=Long.bitCount(el);
			
			while(el > 1) {
				long temp = el % 10;
				if(temp % 2==0) even++;
				else odd++;
				
				el/=10;
			}
		}		
	}
}