package telianova13;

import telianova07.Applicant;
import telianova10.Container;

	public class MyThread implements Runnable {
	    private Container<Applicant> container;
	    private boolean isActive;
	    Thread thread;

	    public MyThread(Container<Applicant> container2, String name){
	        container = container2;
	        isActive = true;
	        thread = new Thread(this, name);
	    }

	    void disable() {
	        isActive = false;
	    }

	    @Override
	    public void run() {
	        int count = count();

	        System.out.println(Thread.currentThread().getName() + ": " + count);
	        System.out.println(Thread.currentThread().getName() + " finished");
	    }

	    public int count() {
	        int count = 0;
	        int minSalary = 0;
	        for(Applicant i : container) {
	            if(isActive) {
	                minSalary = i.getJobRequirements().getMinSalary();
	                if(minSalary > 10000) {
	                    count++;
	                }
	            }
	            else {
	                break;
	            }
	        }
	        return count;
	    }
	}
