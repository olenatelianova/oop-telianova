package telianova12;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import telianova07.Applicant;
import telianova07.JobRequirements;
import telianova07.WorkExperience;
import telianova10.Container;
import telianova03.TypeString;
public class Main {

		    public static void main(String[] args) {
		        Container<Applicant> recruitmentAgency = new Container<Applicant>();

		        for (String str : args) {
		            if(str.equals("-a") || str.equals("-auto")) {
		            	recruitmentAgency = auto(recruitmentAgency);
		                return;
		            }
		        }
		        recruitmentAgency = menu(recruitmentAgency);
		    }

		    private static Container<Applicant> auto(Container<Applicant> recruitmentAgency) {
		        System.out.println("Adding elements...");

		        File file = new File("recruitmentAgency11.txt");

		        try {
		            String education;
		            int day;
		            int month;
		            int year;
		            String specializationPrevious;
		            int experience;
		            String specializationNext;
		            int minSalary;
		            String conditions;
		            Scanner reader = new Scanner(file);
		            while(reader.hasNextLine()) {
		                String data = reader.nextLine();
		                Pattern pattern = Pattern.compile("((\\w+(|\\s))*,\\s([1-9]|[12]\\d|3[01])\\.([1-9]|1[012])\\.((19|20)\\d{2}),\\s" +
		                        "(\\w+.)+,\\s([0-9]|[1-6][0-9]),\\s(\\w+.)+,\\s([1-9]\\d{3,}),\\s(\\w+(\\.|\\s)(\\s|))+)");
		                Matcher matcher = pattern.matcher(data);
		                if(matcher.matches()) {
		                    String[] information = data.split(",\\s");
		                    education = information[0];
		                    specializationPrevious = information[2];
		                    experience = Integer.parseInt(information[3]);
		                    specializationNext = information[4];
		                    minSalary = Integer.parseInt(information[5]);
		                    conditions = information[6];
		                    String[] date = information[1].split("\\.");
		                    day = Integer.parseInt(date[0]);
		                    month = Integer.parseInt(date[1]);
		                    year = Integer.parseInt(date[2]);

		                    int id = recruitmentAgency.getSize();

		                    WorkExperience workExperienceAdd = new WorkExperience(specializationPrevious, experience);
		                    JobRequirements jobRequirementsAdd = new JobRequirements(specializationNext,minSalary,conditions);
		                    Applicant applicantAdd = new Applicant(id++,education,day,month,year,workExperienceAdd,jobRequirementsAdd);
		                    recruitmentAgency.add(applicantAdd);
		                }
		            }
		            reader.close();
		            System.out.println("Adding was end.\n");
		        } catch (FileNotFoundException e){
		            e.printStackTrace();
		        }

		        System.out.println("List in recruitment agency:\n");
		        if(recruitmentAgency.getSize() > 0) {
		            for(var element : recruitmentAgency) {
		                element.print();
		            }
		        }
		        else {
		            System.out.println("The recruitment agency is empty!\n");
		        }

		        Container<Applicant> recruters = task(recruitmentAgency);

		        if(recruters.getSize() > 0) {
		            System.out.println("\nChallangers with wishes to dose not have a buiness trip:\n");
		            for(var challanger : recruters) {
		                challanger.print();
		            }
		            System.out.println();
		        }
		        else {
		            System.out.println("\nChallangers without wishes to dose not have a buisness trip.\n");
		        }

		        int orderSort = 1;

		        recruitmentAgency.sort(new workExperienceComparator(), orderSort);
		        System.out.println("Data sorted by work experience");

		        System.out.println("List in Recruiting Agency:\n");
		        if(recruitmentAgency.getSize() > 0) {
		            for(var element : recruitmentAgency) {
		                element.print();
		            }
		        }

		        return recruitmentAgency;
		    }

		    private static Container<Applicant> menu(Container<Applicant> recruitmentAgency) {
		        boolean endprog = false;
		        Scanner inInt = new Scanner(System.in);
		        Scanner inStr = new Scanner(System.in);
		        int menu;
		        int menuSort;
		        int orderSort;
		        int menuSerialization;
		        int menuDeserialization;

		        while(!endprog)
		        {
		            System.out.println("1. Show all challanger");
		            System.out.println("2. Add challanger");
		            System.out.println("3. Delete chellanger");
		            System.out.println("4. Clear list");
		            System.out.println("5. Is empty recruiting agency?");
		            System.out.println("6. Sort data");
		            System.out.println("7. Serialize data");
		            System.out.println("8. Deserialize data");
		            System.out.println("9. Task");
		            System.out.println("10. Exit");
		            System.out.print("Enter option: ");
		            try
		            {
		                menu =  inInt.nextInt();
		            }
		            catch(java.util.InputMismatchException e)
		            {
		                System.out.println("Error! Wrong input.");
		                endprog = true;
		                menu = 0;
		            }
		            System.out.println();
		            switch(menu)
		            {
		                case 1:
		                    if(recruitmentAgency.getSize() > 0) {
		                        for(var element : recruitmentAgency) {
		                            element.print();
		                        }
		                    }
		                    else {
		                        System.out.println("The recruitment agency is empty!\n");
		                    }
		                    break;
		                case 2:
		                    String education;
		                    int day;
		                    int month;
		                    int year;
		                    String specializationPrevious;
		                    int experience;
		                    String specializationNext;
		                    int minSalary;
		                    String conditions;

		                    Pattern patternEducation = Pattern.compile("(\\w+.)+");
		                    Pattern patternDay = Pattern.compile("([1-9]|[12]\\d|3[01])");
		                    Pattern patternMonth = Pattern.compile("([1-9]|1[012])");
		                    Pattern patternYear = Pattern.compile("(19|20)\\d{2}");
		                    Pattern patternSpecialization = Pattern.compile("(\\w+.)+");
		                    Pattern patternExperience = Pattern.compile("[0-9]|[1-6][0-9]");
		                    Pattern patternMinSalary = Pattern.compile("(^[1-9]\\d{3,})");
		                    Pattern patternConditions = Pattern.compile("(\\w+(\\.|\\s)(\\s|))+");

		                    System.out.println("Enter education of challanger: ");
		                    try {
		                        education = inStr.nextLine();
		                        education = stringRegexCheck(education, patternEducation);
		                    }catch(java.util.InputMismatchException e) {
		                        System.out.println("Error! Incorect input!");
		                        break;
		                    }

		                    System.out.println("Enter day of dismissal: ");
		                    try {
		                        day = inInt.nextInt();
		                        day = intRegexCheck(day, patternDay);
		                    } catch(java.util.InputMismatchException e) {
		                        System.out.println("Error! Incorect input!");
		                        break;
		                    }

		                    System.out.println("Enter month of dismissal: ");
		                    try {
		                        month = inInt.nextInt();
		                        month = intRegexCheck(month, patternMonth);
		                    } catch(java.util.InputMismatchException e) {
		                        System.out.println("Error! Incorect input!");
		                        break;
		                    }

		                    System.out.println("Enter year of dismissal: ");
		                    try {
		                        year = inInt.nextInt();
		                        year = intRegexCheck(year, patternYear);
		                    } catch(java.util.InputMismatchException e) {
		                        System.out.println("Error! Incorect input!");
		                        break;
		                    }

		                    System.out.println("Enter pervious job: ");
		                    try {
		                        specializationPrevious = inStr.nextLine();
		                        specializationPrevious = stringRegexCheck(specializationPrevious, patternSpecialization);
		                    } catch(java.util.InputMismatchException e) {
		                        System.out.println("Error! Incorect input!");
		                        break;
		                    }

		                    System.out.println("Enter experience of working: ");
		                    try {
		                        experience = inInt.nextInt();
		                        experience = intRegexCheck(experience, patternExperience);
		                    } catch(java.util.InputMismatchException e){
		                        System.out.println("Error! Incorect input!");
		                        break;
		                    }

		                    System.out.println("Enter next job: ");
		                    try {
		                        specializationNext = inStr.nextLine();
		                        specializationNext = stringRegexCheck(specializationNext, patternSpecialization);
		                    } catch(java.util.InputMismatchException e) {
		                        System.out.println("Error! Incorect input!");
		                        break;
		                    }

		                    System.out.println("Enter min salary: ");
		                    try {
		                        minSalary = inInt.nextInt();
		                        minSalary = intRegexCheck(minSalary, patternMinSalary);
		                    }catch (java.util.InputMismatchException e) {
		                        System.out.println("Error! Incorect input!");
		                        break;
		                    }

		                    System.out.println("Enter whishes to the next job: ");
		                    try {
		                        conditions = inStr.nextLine();
		                        conditions = stringRegexCheck(conditions, patternConditions);
		                    } catch(java.util.InputMismatchException e){
		                        System.out.println("Error! Incorect input!");
		                        break;
		                    }
		                    int id = recruitmentAgency.getSize();

		                    WorkExperience workExperienceAdd = new WorkExperience(specializationPrevious, experience);
		                    JobRequirements jobRequirementsAdd = new JobRequirements(specializationNext,minSalary,conditions);
		                    Applicant applicantAdd = new Applicant(id++,education,day,month,year,workExperienceAdd,jobRequirementsAdd);
		                    recruitmentAgency.add(applicantAdd);
		                    break;
		                case 3:
		                    System.out.println("Enter ID to delete: ");
		                    int delete = inInt.nextInt();
		                    boolean isExist = false;
		                    if(recruitmentAgency.getSize() > 0) {
		                        for(var element : recruitmentAgency) {
		                            if(element.getRegistrationNum() == delete) {
		                                isExist = true;
		                            }
		                        }
		                        if(isExist) {
		                            if(recruitmentAgency.delete(delete))
		                                System.out.println("Applicant was deleted successfully.");
		                            else
		                                System.out.println("Error! Wrong ID.");
		                        }
		                        else
		                            System.out.println("Error! Wrong ID.");
		                    }
		                    break;
		                case 4:
		                	recruitmentAgency.clear();
		                    System.out.println("recruitment agency is empty now.\n");
		                    break;
		                case 5:
		                    if(recruitmentAgency.isEmpty())
		                        System.out.println("recruitment agency is empty.\n");
		                    else
		                        System.out.println("recruitment agency is not empty.");
		                    break;
		                case 6:
		                    System.out.println("1. Sort by Registration Number");
		                    System.out.println("2. Sort by work experience");
		                    System.out.println("3. Sort by demand to min salary");
		                    System.out.println("4. Return to menu");
		                    System.out.println("Enter option: ");
		                    try
		                    {
		                        menuSort =  inInt.nextInt();
		                    }
		                    catch(java.util.InputMismatchException e)
		                    {
		                        System.out.println("Error! Wrong input.");
		                        break;
		                    }
		                    System.out.println();
		                    System.out.println("How to sort data?");
		                    System.out.println("1. Asc");
		                    System.out.println("2. Desc");
		                    System.out.println("Enter option: ");
		                    try
		                    {
		                        orderSort =  inInt.nextInt();
		                    }
		                    catch(java.util.InputMismatchException e)
		                    {
		                        System.out.println("Error! Wrong input.");
		                        break;
		                    }
		                    switch(menuSort) {
		                        case 1:
		                        	recruitmentAgency.sort(new idComparator(), orderSort);
		                            System.out.println("Data sorted by Registration Number\n");
		                            break;
		                        case 2:
		                        	recruitmentAgency.sort(new workExperienceComparator(), orderSort);
		                            System.out.println("Data sorted by work experience\n");
		                            break;
		                        case 3:
		                        	recruitmentAgency.sort(new minSalazyComparator(), orderSort);
		                            System.out.println("Data sorted by demand to min salary");
		                            break;
		                        case 4:

		                            break;
		                        default:
		                            System.out.println("Error! Wrong num in Sort menu.");
		                            break;
		                    }
		                    break;
		                case 7:
		                    String filenameSerialization;
		                    String filenameXML;

		                    System.out.println("1. Serialization");
		                    System.out.println("2. XML serialization");
		                    System.out.println("0. Exit serialization");
		                    try
		                    {
		                        menuSerialization =  inInt.nextInt();
		                    }
		                    catch(java.util.InputMismatchException e)
		                    {
		                        System.out.println("Error! Wrong input.");
		                        menuSerialization = 0;
		                    }
		                    switch(menuSerialization)
		                    {
		                        case 1:
		                            System.out.println("\nEnter file name: ");
		                            filenameSerialization = inStr.nextLine();
		                            if (filenameSerialization.indexOf(".ser") == -1) {
		                                filenameSerialization += ".ser";
		                            }
		                            try(ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream (filenameSerialization)))){
		                                oos.writeObject(recruitmentAgency);
		                                System.out.println("Serialization successful.");
		                            } catch (Exception e){
		                                System.out.println(e.getMessage());
		                            }
		                            break;
		                        case 2:
		                            System.out.print("Enter XML filename: ");
		                            filenameXML = inStr.nextLine();
		                            if (filenameXML.indexOf(".xml") == -1)
		                                filenameXML += ".xml";
		                            try(XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream (filenameXML)))){
		                                encoder.writeObject(recruitmentAgency);
		                                System.out.println("Serialization successful.");
		                            } catch (Exception e){
		                                System.out.println(e.getMessage());
		                            }
		                            break;
		                        case 0:
		                            break;
		                        default:
		                            System.out.println("Error! Wrong num in menu.");
		                            break;
		                    }
		                    break;
		                case 8:
		                    String filenameDeserialization;

		                    System.out.println("1. Deserialization");
		                    System.out.println("2. XML deserialization");
		                    System.out.println("0. Exit deserialization");
		                    try
		                    {
		                        menuDeserialization =  inInt.nextInt();
		                    }
		                    catch(java.util.InputMismatchException e)
		                    {
		                        System.out.println("Error! Wrong input.");
		                        menuDeserialization = 0;
		                    }
		                    switch(menuDeserialization)
		                    {
		                        case 1:
		                            System.out.println("\nEnter file name: ");
		                            filenameDeserialization = inStr.nextLine();
		                            if (filenameDeserialization.indexOf(".ser") == -1) {
		                                filenameDeserialization += ".ser";
		                            }
		                            try(ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream (filenameDeserialization)))){
		                            	recruitmentAgency.clear();
		                            	recruitmentAgency = (Container<Applicant>) ois.readObject();
		                                System.out.println("Deserialization successful.");
		                            } catch (Exception e){
		                                System.out.println(e.getMessage());
		                            }
		                            break;
		                        case 2:
		                            System.out.print("Enter XML filename: ");
		                            filenameDeserialization = inStr.nextLine();
		                            if (filenameDeserialization.indexOf(".xml") == -1)
		                                filenameDeserialization += ".xml";
		                            try(XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream (filenameDeserialization)))){
		                            	recruitmentAgency.clear();
		                            	recruitmentAgency = (Container<Applicant>) decoder.readObject();
		                                System.out.println("Deserialization successful.");
		                            } catch (Exception e){
		                                System.out.println(e.getMessage());
		                            }
		                            break;
		                        case 0:
		                            break;
		                        default:
		                            System.out.println("Error! Wrong num in menu.");
		                            break;
		                    }
		                    break;
		                case 9:
		                    Container<Applicant> recruters = task(recruitmentAgency);

		                    if(recruters.getSize() > 0) {
		                        System.out.println("\nApplicant with wishes to dose not have a buiness trip:\n");
		                        for(var challanger : recruters) {
		                            challanger.print();
		                        }
		                        System.out.println();
		                    }
		                    else {
		                        System.out.println("\nApplicants without wishes to dose not have a buisness trip.\n");
		                    }
		                    break;
		                case 0:
		                    endprog = true;
		                    inInt.close();
		                    inStr.close();
		                    break;
		                default:
		                    System.out.println("Error! Wrong num in menu.");
		                    break;
		            }
		        }
		        return recruitmentAgency;
		    }
		    public static int intRegexCheck(int value, Pattern pattern)
		    {
		        Matcher matcher;
		        Scanner in = new Scanner(System.in);
		        boolean ready = false;
		        do
		        {
		            matcher = pattern.matcher(Integer.toString(value));
		            if(!matcher.matches())
		            {
		                System.out.println("You've entered the wrong data. Try again:");
		                value = in.nextInt();
		            }
		            else
		                ready = true;
		        }
		        while(!ready);
		        return value;
		    }

		    public static String stringRegexCheck(String value, Pattern pattern)
		    {
		        Matcher matcher;
		        Scanner in = new Scanner(System.in);
		        boolean ready = false;
		        do
		        {
		            matcher = pattern.matcher(value);
		            if(!matcher.matches())
		            {
		                System.out.println("You've entered the wrong data. Try again:");
		                value = in.nextLine();
		            }
		            else
		                ready = true;
		        }
		        while(!ready);
		        return value;
		    }

		    public static Container<Applicant> task(Container<Applicant> recruitmentAgency) {
		        String conditions;
		        String prevJob;
		        Pattern patternManager = Pattern.compile(".*(M|m)anager.*");
		        Pattern patternNot = Pattern.compile(".*(N|n)ot.*");
		        Pattern patternBuisnessTrip = Pattern.compile(".*(B|b)uisness trip.*");
		        Container<Applicant> task = new Container<Applicant>();

		        if(recruitmentAgency.getSize() > 0) {
		            for(var element : recruitmentAgency) {
		                conditions = element.getJobRequirements().getConditions();
		                prevJob = element.getWorkExperience().getSpecialization();
		                Matcher matcher = patternManager.matcher(prevJob);
		                if(matcher.matches()) {
		                    Matcher matcherNot = patternNot.matcher(conditions);
		                    if(matcherNot.matches()) {
		                        Matcher matcherBuisnessTrip = patternBuisnessTrip.matcher(conditions);
		                        if(matcherBuisnessTrip.matches()) {
		                            task.add(element);
		                        }
		                    }
		                }
		            }
		        }
		        return task;
		    }
		}
