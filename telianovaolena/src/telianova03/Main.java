package telianova03;
import java.lang.String;
import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		String[] str1 = new String[]{"Tree", "Max", "Box", "Mouse", "Book", "Ring", "Lemon", "Cheese", "Pony", "Poltava", "Alphabet", "Zoo"};
        System.out.println("Original string:");
        ShowStringMass(str1);
        Arrays.sort(str1);
        System.out.println("\nSort by alphabet:");
        ShowStringMass(str1);
        LengthSort sort1 = new LengthSort();
        Arrays.sort(str1, sort1);
        System.out.println("\nSort by lenght:");
        ShowStringMass(str1);
        String str2 = new String("From blood and ash");
        StringBuilder strBLD2 = new StringBuilder(str2);
        System.out.print("\n\nUsing StringBuilder:");
        System.out.println("\nOriginal string:");
        System.out.println(strBLD2);
        strBLD2.append(".  We have risen.");
        System.out.println("\nTransformation with StringBuilder:");
        System.out.println(strBLD2);
    }

    private static void ShowStringMass(String[] str){
        for (int i = 0; i< str.length; i++)
            System.out.print(str[i]+" ");
    }


	}

