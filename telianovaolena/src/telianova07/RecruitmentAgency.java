package telianova07;

public class RecruitmentAgency {
	Applicant[] mas = new Applicant[3];

    /**
     * Метод вывода массива данных
     */

    public void printAll() {
        for(int i = 0 ; i < mas.length; i++) {
            System.out.println("ID: " + mas[i].getRegistrationNum());
            System.out.println("Education: " + mas[i].getEducation());
            System.out.println("Release date: " + mas[i].getDismissalDay()+"/"+mas[i].getDismissalMonth()+"/"+mas[i].getDismissalYear());
            System.out.println("Experience");
            System.out.println("Place of past work: " + mas[i].getWorkExperience().getSpecialization());
            if(mas[i].getWorkExperience().getExperience() <= 4)
                System.out.println("Experience: " + mas[i].getWorkExperience().getExperience() + "year");
            else
                System.out.println("Experience: " + mas[i].getWorkExperience().getExperience() + "years");
            System.out.println("Desire for work" );
            if(mas[i].getJobRequirements().getMinSalary() == 0 && mas[i].getJobRequirements().getSpecialization() == null && mas[i].getJobRequirements().getConditions() == null)
                System.out.println("The candidate does not have any aspirations for future work");
            else {
                if( mas[i].getJobRequirements().getMinSalary() != 0)
                    System.out.println("Desired minimum salary: " +  mas[i].getJobRequirements().getMinSalary());
                else
                    System.out.println("Desired minimum salary: The candidate does not have any wishes regarding the salary " );
                if(mas[i].getJobRequirements().getSpecialization() != null)
                    System.out.println("Desired future work: " + mas[i].getJobRequirements().getSpecialization());
                else
                    System.out.println("Desired future work: The applicant does not have any wishes for the job");
                if(mas[i].getJobRequirements().getConditions() != null)
                    System.out.println("Desired conditions of future work: " + mas[i].getJobRequirements().getConditions());
                else
                    System.out.println("Desired conditions of future work: The applicant does not have any wishes for the job");
            }
            System.out.println("---------------------");
        }
    }
}