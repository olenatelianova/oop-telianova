package telianova07;

public class Main {

    public static void main(String[] args) {
    	RecruitmentAgency list = new RecruitmentAgency();
        WorkExperience workExperience1 = new WorkExperience("School",4);
        JobRequirements jobRequirements1 = new JobRequirements("Kindergarten",7800,"Medical Insurance.");
        list.mas[0] = new Applicant(39821,"Higher Education",13,05,2020,workExperience1,jobRequirements1);
        workExperience1 = new WorkExperience("Hospital",14);
        jobRequirements1 = new JobRequirements(null,0,null);
        list.mas[1] = new Applicant(15301,"Higher Education",15,10,2014, workExperience1,jobRequirements1);
        workExperience1 = new WorkExperience("University",38);
        jobRequirements1 = new JobRequirements("Teacher",15000,"Medical Insurance.");
        list.mas[2] = new Applicant(3004,"secondary education",14,03,2020, workExperience1,jobRequirements1);
        list.printAll();
        list.mas[2].getJobRequirements().setConditions("Lunch breaks.");
        list.mas[1].getJobRequirements().setMinSalary(9300);
        list.printAll();
    }

}
