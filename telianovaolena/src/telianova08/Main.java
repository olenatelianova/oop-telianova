package telianova08;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashSet;
import java.util.Scanner;

final class Main {
    private Main() {
        //default constructor
    }

    public static void main(final String[] args) throws IOException {
        Kontainer contains = new Kontainer();
        RecruitmentAgency rec1 = new RecruitmentAgency();
        Scanner scan = new Scanner(System.in);
        String str = "";
        String str3 = "";
        boolean loop = true;
        while (loop) {
            Functions.chooseMenu();
            int choose = scan.nextInt();
            switch (choose) {
                case 1:
                    rec1.generateVacancy();
                    contains.add(rec1);
                    System.out.println("Successfully!");
                    break;
                case 2:
                    if (contains.size() == 0) {
                        System.out.println("Error! Array is empty!");
                        break;
                    } else {
                        contains.show();
                        break;
                    }
                case 3:
                    if (contains.size() == 0) {
                        System.out.println("Error! Array is empty!");
                        break;
                    } else {
                        System.out.print("Enter a delete position from 1 to " + contains.size());
                        int index = scan.nextInt();
                        if (index > contains.size() || index < 1) {
                            System.out.println("Retype: ");
                        } else {
                            contains.remove(index);
                            System.out.println("Successfully!");
                        }
                    }
                case 4:
                    if (contains.size() == 0) {
                        System.out.println("Error! Array is empty!");
                        break;
                    } else {
                        contains.clear();
                        System.out.println("Successfully!");
                        break;
                    }
                case 5:
                    DocumentBuilderFactory factory = DocumentBuilderFactory.newDefaultInstance();
                    try {
                        DocumentBuilder builder = factory.newDocumentBuilder();
                        Document document = builder.parse("test.xml");
                        Node root = document.getDocumentElement();
                        System.out.println("Список вакансий: ");
                        System.out.println();
                        NodeList vacancies = root.getChildNodes();
                        for (int i = 0; i < vacancies.getLength(); i++) {
                            Node vacancy = vacancies.item(i);
                            if (vacancy.getNodeType() != Node.TEXT_NODE) {
                                NodeList bookProps = vacancy.getChildNodes();
                                for (int j = 0; j < bookProps.getLength(); j++) {
                                    Node bookProp = bookProps.item(j);
                                    if (bookProp.getNodeType() != Node.TEXT_NODE) {
                                        System.out.println(bookProp.getNodeName() + ":" + bookProp.getChildNodes().item(0).getTextContent());
                                    }
                                }
                                System.out.println("===========>>>>");
                            }
                        }

                    } catch (ParserConfigurationException | SAXException | IOException ex) {
                        ex.printStackTrace(System.out);
                    }
                    break;
                case 6:
                    boolean loop2 = true;
                    while (loop2) {
                        Functions.chooseMenuFile();
                        int choose2 = scan.nextInt();
                        switch (choose2) {
                            case 1:
                                contains.save();
                                System.out.println("Successfully!");
                                break;
                            case 2:
                                Files.walkFileTree(Paths.get("."), new HashSet<FileVisitOption>(), 1, new FileVisitor());
                                break;
                            case 3:
                                Path path = Paths.get("save.txt").toAbsolutePath();
                                System.out.println(path.getParent());
                                break;
                            case 4:
                                boolean loop5 = true;
                                while (loop5) {
                                    int cho = 0;
                                    System.out.println("Enter directory path: ");
                                    str = scan.next();
                                    Files.walkFileTree(Paths.get(str), new HashSet<FileVisitOption>(), 1, new FileVisitor());
                                    System.out.println("===========================================");
                                    System.out.println("Do you want to save the file to this directory?");
                                    System.out.println("1. - Yes. 0. - No.");
                                    while (true) {
                                        cho = scan.nextInt();
                                        if (cho == 1) {
                                            str3 = str.concat("\\save.txt");
                                            contains.save(str3);
                                            loop5 = false;
                                            break;
                                        } else if (cho == 0) {
                                            break;
                                        } else {
                                            System.out.println("Error try again: ");
                                        }
                                    }
                                }
                                break;
                            case 5:
                                System.out.println("Did you enter your own directory path?");
                                System.out.println("1 - Yes. 0 - No.");
                                boolean loop4 = true;
                                while (loop4) {
                                    int chosik = scan.nextInt();
                                    if (chosik == 1) {
                                        System.out.println("Enter a new file name: ");
                                        String str1 = scan.next();
                                        String str4 = str;
                                        str4 = str4.concat("\\");
                                        str4 = str4.concat(str1);
                                        Files.move(Paths.get(str3), Paths.get(str4), StandardCopyOption.REPLACE_EXISTING);
                                        System.out.println("Successfully!");
                                        loop4 = false;
                                    } else if (chosik == 0) {
                                        System.out.println("Follow step 4");
                                        loop4 = false;
                                    } else {
                                        System.out.println("Error! Retype: ");
                                    }
                                }
                                break;
                            case 6:
                                contains.read();
                                System.out.println("Successfully!");
                                break;
                            case 0:
                                System.out.println("Exit!");
                                loop2 = false;
                                break;
                            default:
                                System.out.println("Error. Wrong number!");
                                break;
                        }
                    }
                    break;
                case 0:
                    System.out.println("Thank you for your work!");
                    loop = false;
                    break;
                default:
                    System.out.println("Error. Wrong number!");
                    break;
            }
        }
    }
}
