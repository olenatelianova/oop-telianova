package telianova08;
import java.util.Scanner;

public class RecruitmentAgency  {
    private String firm;
    private String specialty;
    private String workingConditions;
    private int payment;

    private String requiredSpeciality;
    private int experience;
    private String education;

    private boolean confirm = false;

    public void setFirm(final String firm) {
        this.firm = firm;
    }

    public void setSpecialty(final String specialty) {
        this.specialty = specialty;
    }

    public void setWorkingConditions(final String workingConditions) {
        this.workingConditions = workingConditions;
    }

    public void setPayment(final int payment) {
        this.payment = payment;
    }

    public void setConfirm(final boolean confirm) {
        this.confirm = confirm;
    }

    public int getPayment() {
        return payment;
    }

    public String getWorkingConditions() {
        return workingConditions;
    }

    public String getSpecialty() {
        return specialty;
    }

    public String getFirm() {
        return firm;
    }

    public RecruitmentAgency() {
        firm = null;
        specialty = null;
        workingConditions = null;
        payment = 0;
        requiredSpeciality = null;
        experience = 0;
        education = null;
        confirm = false;

    }

    RecruitmentAgency(final RecruitmentAgency obj) {
        firm = obj.firm;
        specialty = obj.specialty;
        workingConditions = obj.workingConditions;
        payment = obj.payment;
        requiredSpeciality = obj.requiredSpeciality;
        experience = obj.experience;
        education = obj.education;
        confirm = obj.confirm;
    }

    void generateVacancy() {
        Scanner scan = new Scanner(System.in);
        Scanner scan2 = new Scanner(System.in);
        int choose;
        System.out.print("\nEnter company: ");
        firm = scan.nextLine();
        System.out.print("\nEnter specialty: ");
        specialty = scan.nextLine();
        System.out.print("\nEnter working conditions: ");
        workingConditions = scan.nextLine();
        System.out.print("\nEnter payment: ");
        payment = scan.nextInt();
        System.out.println("Would you like to add the conditions of the work? 1 - Yes. 0 - No");
        choose = scan.nextInt();
        while (true) {
            if (choose == 1) {
                System.out.print("\nEnter required speciality: ");
                requiredSpeciality = scan2.nextLine();
                System.out.print("\nEnter necessary education: ");
                education = scan2.nextLine();
                System.out.print("\nEnter necessary experience: ");
                experience = scan2.nextInt();
                confirm = true;
                break;
            } else if (choose == 0) {
            	requiredSpeciality = null;
                experience = 0;
                education = null;
                break;
            } else {
                System.out.println("Error! Please re-enter: ");
            }
        }
    }

    public void setExperience(final int experience) {
        this.experience = experience;
    }

    public int getExperience() {
        return experience;
    }

    public void setRequiredSpeciality(final String needsSpeciality) {
        this.requiredSpeciality = needsSpeciality;
    }

    public String getRequiredSpeciality() {
        return requiredSpeciality;
    }

    public void setEducation(final String education) {
        this.education = education;
    }

    public String getEducation() {
        return education;
    }

    public boolean getConfirms() {
        return confirm;
    }

    @Override
    public String toString() {
        return "Recruitment{" +
                "firm='" + firm + '\'' +
                ", specialty='" + specialty + '\'' +
                ", workingConditions='" + workingConditions + '\'' +
                ", payment=" + payment +
                ", requiredSpeciality='" + requiredSpeciality + '\'' +
                ", experience=" + experience +
                ", education='" + education + '\'' +
                ", confirm=" + confirm +
                '}';
    }
}
