package telianova08;

final class Functions {

    private Functions() {
    }

    static void chooseMenu() {
        System.out.println("\n\nSelect the required option:");
        System.out.println("1. Add vacancy");
        System.out.println("2. Show current vacancies");
        System.out.println("3. Delete vacancies");
        System.out.println("4. Clear job list");
        System.out.println("5. Use XML");
        System.out.println("6. Working with files");
        System.out.println("0. Finish work");
        System.out.print("Select: ");
    }

    static void chooseMenuFile() {
        System.out.println("\n\nSelect the required option:");
        System.out.println("1. Save file to current directory");
        System.out.println("2. View directory contents");
        System.out.println("3. View tree to file");
        System.out.println("4. Save the file to the required path");
        System.out.println("5. Rename file");
        System.out.println("6. Read file from memory");
        System.out.println("0. Finish work");
        System.out.print("Select: ");
    }
}
