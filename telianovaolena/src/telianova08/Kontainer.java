package telianova08;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Arrays;

class Kontainer implements Serializable {

    private final int size = 10;
    private int count = 0;

    private RecruitmentAgency[] abs = new RecruitmentAgency[size];


    void add(final RecruitmentAgency temp) {
        RecruitmentAgency buff = new RecruitmentAgency(temp);
        if (count == abs.length) {
            abs = Arrays.copyOf(abs, abs.length * 2);
            abs[count++] = buff;
        } else {
            abs[count++] = buff;
        }
    }

    void save() {
        try {
            File file = new File("save.txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            PrintWriter pw = new PrintWriter(file);
            System.out.println();
            for (int i = 0; i < count; i++) {
                pw.println(count);
                pw.println(abs[i].getFirm());
                pw.println(abs[i].getSpecialty());
                pw.println(abs[i].getWorkingConditions());
                pw.println(abs[i].getPayment());
                pw.println(abs[i].getConfirms());
                if (abs[i].getConfirms()) {
                    pw.println(abs[i].getRequiredSpeciality());
                    pw.println(abs[i].getExperience());
                    pw.println(abs[i].getEducation());
                }
            }
            pw.close();
        } catch (IOException e) {
            System.out.println("Error" + e);
        }
    }

    void saveInDirectory() {
        String str = "";

        File file = new File("save.txt");
    }

    void save(final String str) {
        try {
            File file = new File(str);
            if (!file.exists()) {
                file.createNewFile();
            }
            PrintWriter pw = new PrintWriter(file);
            System.out.println();
            for (int i = 0; i < count; i++) {
                pw.println(count);
                pw.println(abs[i].getFirm());
                pw.println(abs[i].getSpecialty());
                pw.println(abs[i].getWorkingConditions());
                pw.println(abs[i].getPayment());
                pw.println(abs[i].getConfirms());
                if (abs[i].getConfirms()) {
                    pw.println(abs[i].getRequiredSpeciality());
                    pw.println(abs[i].getExperience());
                    pw.println(abs[i].getEducation());
                }
            }
            pw.close();
        } catch (IOException e) {
            System.out.println("Error" + e);
        }
    }

    void read() throws IOException {
        BufferedReader br = null;
        RecruitmentAgency temp = new RecruitmentAgency();
        try {
            br = new BufferedReader(new FileReader("save.txt"));
            String line;
            line = br.readLine();
            count = Integer.parseInt(line);
            for (int i = 0; i < count; i++) {
                line = br.readLine();
                temp.setFirm(line);
                line = br.readLine();
                temp.setSpecialty(line);
                line = br.readLine();
                temp.setWorkingConditions(line);
                line = br.readLine();
                temp.setPayment(Integer.parseInt(line));
                line = br.readLine();
                temp.setConfirm(Boolean.parseBoolean(line));
                if (temp.getConfirms()) {
                    line = br.readLine();
                    temp.setRequiredSpeciality(line);
                    line = br.readLine();
                    temp.setExperience(Integer.parseInt(line));
                    line = br.readLine();
                    temp.setEducation(line);
                }
                abs[i] = temp;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            br.close();
        }
    }

    void show() {
        if (count == 0) {
            System.out.println("Error! Empty array!");
        } else {
            System.out.println();
            for (int i = 0; i < count; i++) {
                System.out.println("Vacancy #" + (i + 1));
                System.out.println("Firm: " + abs[i].getFirm());
                System.out.println("Speciality: " + abs[i].getSpecialty());
                System.out.println("Working conditions: " + abs[i].getWorkingConditions());
                System.out.println("Payment: " + abs[i].getPayment());
                if (abs[i].getConfirms()) {
                    System.out.println("Required specialty: " + abs[i].getRequiredSpeciality());
                    System.out.println("Experience: " + abs[i].getExperience());
                    System.out.println("Education: " + abs[i].getEducation());
                }
            }
        }
    }

    void clear() {
        for (int i = 0; i < count; i++) {
            abs[i] = null;
        }
        count = 0;
    }

    public void remove(final int index) {
        int temp = index;
        for (int i = temp; i < count; i++) {
            abs[temp++] = abs[i + 1];
        }
        abs[count] = null;
        count--;
    }

    int size() {
        return count;
    }
}
